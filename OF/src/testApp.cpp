#include "testApp.h"

std::string trim(const std::string& str,
                 const std::string& whitespace = " \t")
{
    const size_t strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const size_t strEnd = str.find_last_not_of(whitespace);
    const size_t strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

// sort BlobInfo by Y position
bool sort_fun (const BlobInfoSt& x,const BlobInfoSt& y) {
    return x.rad < y.rad;
}


bool testApp::loadSettings(const string &inFilename)
{
    bool bSuccess = false;

    ofFile file(inFilename);
    if (file.exists()) {
        stringstream ss;

        enum SettingLoaders {
            SENDER = 0,
            CAMERA,
            MIN_AREA,
            MAX_AREA,
            THRESHOLD,
            LOW_OCT,
            WHITE_BALANCE,
            WORKSPACE_CORNERS
        };

        int whichSetting = 0;
        for( string line; getline(file, line) && !bSuccess; ) {
            line = trim(line);
            if ( !line.empty() && line[0] != '#' ) {
                ss.str(line);
                ss.clear();
                switch (whichSetting++) {
                    case SENDER: {
                        ss >> mSenderAddr >> mSenderPort;
                    } break;
                    case CAMERA: {
                        ss >> mCameraId;
                    } break;
                    case MIN_AREA: {
                        ss >> mMinArea;
                    } break;
                    case MAX_AREA: {
                        ss >> mMaxArea;
                    } break;
                    case THRESHOLD: {
                        ss >> mThreshold;
                    } break;
                    case LOW_OCT: {
                        ss >> mLowOctaveThresh;
                    } break;
                    case WHITE_BALANCE: {
                        ss >> mWBScaleRed >> mWBScaleGreen >> mWBScaleBlue;
                    } break;
                    case WORKSPACE_CORNERS: {
                        for (size_t i = 0; i < 4; i++) {
                            ss >> mWorkspaceCorners[i].x >> mWorkspaceCorners[i].y;
                        }
                        bSuccess = true;
                    } break;
                    default:
                        ofLog() << "Case " << whichSetting << " not handled!";
                        break;
                }
            }
        }
        // Now load the color database
        enum ColorLoaders {
            NAME = 0,
            DEGREE,
            HUE,
            SAT,
            BRI
        };
        int nColorLoadpoint = 0;
        ofPtr<ColorSpot> pCurColor(new ColorSpot());
        for( string line; getline(file, line); ) {
            line = trim(line);
            if ( !line.empty() && line[0] != '#' ) {
                double mean, var;
                ss.str(line);
                ss.clear();
                switch (nColorLoadpoint++) {
                    case NAME:
                        ss >> pCurColor->mName;
                        break;
                    case DEGREE:
                        ss >> pCurColor->mDegree;
                        break;
                    case HUE:
                        ss >> mean >> var;
                        pCurColor->mHue.setMeanAndVariance(mean, var);
                        break;
                    case SAT:
                        ss >> mean >> var;
                        pCurColor->mSat.setMeanAndVariance(mean, var);
                        break;
                    case BRI:
                        ss >> mean >> var;
                        pCurColor->mBri.setMeanAndVariance(mean, var);
                        ofLog() << "Loading a color: " << pCurColor->mName << " Degree: " << pCurColor->mDegree;
                        pCurColor->mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussCircularStat(pCurColor->mHue.getMean(), pCurColor->mHue.getVariance())));
                        pCurColor->mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(pCurColor->mSat.getMean(), pCurColor->mSat.getVariance())));
#ifdef USE_BRIGHTNESS_FEATURE
                        pCurColor->mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(pCurColor->mBri.getMean(), pCurColor->mBri.getVariance())));
#endif

                        pCurColor->mClassifier.setProb(0.5);  // Doesn't really matter what we put here for now,
                                                            // as long as they're all the same.
                        mColorData.push_back(*pCurColor);
                        pCurColor = ofPtr<ColorSpot>(new ColorSpot());
                        nColorLoadpoint = 0;  // Now start another color
                        break;
                    default:
                        ofLogWarning() << "Shouldn't have gotten here!";
                        break;
                }
            }
        }
#ifdef USE_BRIGHTNESS_FEATURE
        ofLog() << "Including Brightness as a feature";
#else
        ofLog() << "Not including Brightness as a feature";
#endif

    }
    else {
        ofLogError() << "File " << inFilename << " not found.";
    }

    return bSuccess;
}

void testApp::saveSettings(const string &inFilename)
{
    ofFile file(inFilename, ofFile::WriteOnly);
    file << "# sender_addr server_port" << endl;
    file << mSenderAddr << " " << mSenderPort << endl;
    file << endl;
    file << "# camera device number (0-N) -- index of the webcam to use, normally 0 or 1" << endl;
    file << mCameraId << endl;
    file << endl;
    file << "# minimum blob area in pixels" << endl;
    file << mMinArea << endl;
    file << endl;
    file << "# maximum blob area in pixels" << endl;
    file << mMaxArea << endl;
    file << endl;
    file << "# threshold" << endl;
    file << mThreshold << endl;
    file << endl;
    file << "# above this area (in pixels), we shift down an octave" << endl;
    file << mLowOctaveThresh << endl;
    file << endl;
    file << "# white balance color scaling (Red Green Blue)" << endl;
    file << mWBScaleRed << " " << mWBScaleGreen << " " << mWBScaleBlue << endl;
    file << endl;
    file << "# workspace corners (4 XY coords --> top left, right, bottom right, left -- i.e. clockwise from upper left)" << endl;
    for (size_t i = 0; i < 4; i++) {
        file << "\t\t" << mWorkspaceCorners[i].x << " " << mWorkspaceCorners[i].y;
    }
    file << endl << endl;

    file << "# Stats for colors, in the following format:" << endl;
    file << "#   colorName" << endl;
    file << "#   noteDegree (in scale: 0 = root, 7 = up one octave)" << endl;
    file << "#   hueMean hueVariance (hue is -180 to 180 or 0-360 degrees)" << endl;
    file << "#   saturationMean saturationVariance (sat is 0-255)" << endl;
    file << "#   brightnessMean brightnessVariance (bri is 0-255)" << endl;
    file << endl;

    for (size_t i = 0; i < mColorData.size(); i++) {
        ColorSpot &curColor = mColorData[i];
        file << curColor.mName << endl;
        file << curColor.mDegree << endl;
        file << round(curColor.mHue.getMean()) << " " << curColor.mHue.getVariance() << endl;
        file << round(curColor.mSat.getMean()) << " " << round(curColor.mSat.getVariance()) << endl;
        file << round(curColor.mBri.getMean()) << " " << round(curColor.mBri.getVariance()) << endl;
        file << endl;
    }
    
    ofLog() << "Saved settings file: '" << inFilename << "'";
}

//--------------------------------------------------------------
void testApp::setup()
{

    mSenderAddr = "127.0.0.1";   // Needs to be set to the server's address (using config file)
    mSenderPort = 12345;            // Default SuperCollider language port
    mCameraId = 1;                  // Which camera index to use (normally 1, or 0 if you have no built-in webcam)
    mMinArea = 10 * 10;             // we ignore blobs below this size in pixels
    mMaxArea = 50 * 50;             // we ignore blobs above this size in pixels
    mHighOctaveThresh = 400;        // below this area (in pixels), we shift up an octave
    mLowOctaveThresh = 1200;        // above this area (in pixels), we shift down an octave
    mShowHistograms = false;
    mShowHelp = false;
    
	mCaptureWidth = 640;
	mCaptureHeight = 480;

    mWorkspaceCorners[0].set(0, 0);
    mWorkspaceCorners[1].set(mCaptureWidth, 0);
    mWorkspaceCorners[2].set(mCaptureWidth, mCaptureHeight);
    mWorkspaceCorners[3].set(0, mCaptureHeight);
    mSettingCornerId = -1;
    
    mWBScaleRed = mWBScaleGreen = mWBScaleBlue = 1.0;

    if(bBlackVinyl)
        mThreshold = 120;
    else
        mThreshold = 180;
    

    // Load configuration file
    if (!loadSettings("wuTocataSetup.txt")) {
        ofLogWarning() << "Problem with setup file - OSC may not be correctly configured.";
    }

 	ofBackground(0, 0, 0);
	ofSetFrameRate(20);
	ofSetVerticalSync(false);
	mDrawBlobs = true;
	mMaInvisible = false;
    mStatus = STAT_NONE;
    vShowImage = SHOW_COLOR;

    bSyncActive = true;
	bVinylActive = true;
    bBlackVinyl = true;

    bSyncBlobCrossing = false;
    
    mInput.listDevices();

#ifdef USE_UVC_CAMERA
    string cameraName = "Logitech Camera";
    int vendorId = 0x046d;
    int productId = 0x0825;
    int interfaceNum = 0;

    mInput.setDeviceID(mCameraId);
    mInput.initGrabber(mCaptureWidth, mCaptureHeight);
    ofPtr<ofBaseVideoGrabber> pGrabber = ofPtr<ofBaseVideoGrabber>(mInput.getGrabber());
    ofQTKitGrabber *pG = (ofQTKitGrabber *) pGrabber.get();
    vector<string> availableCams = pG->listVideoDevices();

    for(int i = 0; i < availableCams.size(); i++){
        ofLog() << i << ": " << availableCams[i];
        if(availableCams.at(i) == cameraName){
            mCameraId = i;
        }
    }

//    focus = 0.5;

    mUvcControl.useCamera(vendorId, productId, interfaceNum);
    mUvcControl.setAutoExposure(false);
    mUvcControl.setWhiteBalance(0/10000.0);
    mUvcControl.setAutoWhiteBalance(false);
    vector<ofxUVCControl> controls = mUvcControl.getCameraControls();
    for (int i = 0; i < controls.size(); i++) {
        ofLog() << i << ": " << controls[i].name << " -- " << controls[i].supportsGet();
    }
#else
    mInput.setDeviceID(mCameraId);
	mInput.initGrabber(mCaptureWidth, mCaptureHeight);
	mInput.setDesiredFrameRate(60);
#endif

	mColor.allocate(mCaptureWidth, mCaptureHeight);
	mColorPlus.allocate(mCaptureWidth, mCaptureHeight, OF_IMAGE_COLOR);
	mGray.allocate(mCaptureWidth, mCaptureHeight);
	mGrayLabel.allocate(mCaptureWidth, mCaptureHeight);
	mGrayDiff.allocate(mCaptureWidth, mCaptureHeight);
	mGrayPlus.allocate(mCaptureWidth, mCaptureHeight);
	mFons.allocate(mCaptureWidth, mCaptureHeight);
	mFonsLabel.allocate(mCaptureWidth, mCaptureHeight);

    // Setup OSC communications
    mSenderAddr = "127.0.0.1";   // Needs to be set to the server's address (using config file)
    mSenderPort = 12345;            // Default SuperCollider language port
    mSender.setup(mSenderAddr, mSenderPort);
    ofLog() << "Using server: '" << mSenderAddr << "' on port " << mSenderPort;

    // Other OF setup
    mFont.loadFont("Arial.ttf", 14);
    mFontSmall.loadFont("Arial.ttf", 11);

    for(int i=0; i<kNumLevels; i++) {
        mFracHue[i]=0;
        mFracSat[i]=0;
        mFracBri[i]=0;
    }


}


//--------------------------------------------------------------
void testApp::update()
{
	mInput.update();
    
	if(mInput.isFrameNew()) {

		mColor.setFromPixels(mInput.getPixels(), mCaptureWidth, mCaptureHeight);
        if (mSettingCornerId < 0) {
            // We're not in setting corners mode, so distort to match the rectangle!
            mColor.warpPerspective(mWorkspaceCorners[0], mWorkspaceCorners[1], mWorkspaceCorners[2], mWorkspaceCorners[3]);
        }
		if(mMaInvisible)
		{
		    // si vull eliminar el BLANC
			mGrayPlus.deleteWhite(&mColor);
			mGray.setFromPixels(mGrayPlus.getPixels(), mCaptureWidth, mCaptureHeight);
		}else
            mGray = mColor;

		// take the abs value of the difference between background and incoming and then threshold:
        mGrayDiff = mGray;
        // it makes no sense with a moving image 
		mGrayDiff.threshold(mThreshold);
//        mGrayDiff.blur(7);

        mGrayLabel = mGrayDiff;
        
        if(bVinylActive)
        {
            if(bBlackVinyl)
            {
                mGrayTmp = mFons;
                mGrayTmp.mix(&mGrayDiff, 150, true);
                mGrayDiff = mGrayTmp;
            }else
            {
                mGrayDiff += mFons;
                mGrayDiff.invert();
            }
            
            mContour.findContours(mGrayDiff, 30, (mCaptureWidth*mCaptureHeight)/3, MAX_BLOBS, false);
 
            blobInfo.clear();
            
            for(int j=0; j<mContour.nBlobs;j++) {
                
                BlobInfoSt blobInfoTmp;

                isActive_blob_info( &(mContour.blobs[j]), &(blobInfoTmp));
                
                if(blobInfoTmp.isActive)
                {
                    blobInfoTmp.color = calc_color(&(mContour.blobs[j]), mColor.getPixels(), mGray.getPixels());
                    
                    get_blob_info( &(mContour.blobs[j]), &(blobInfoTmp));
                    
                    blobInfo.push_back(blobInfoTmp);
                }
            }
            
            // order blobs by y position
            ofSort(blobInfo, sort_fun);
            
            // ENVIO INFO A SC via OSC
            if(mStatus == STAT_READY_SEND)
            {
                for (size_t i = 0; i < blobInfo.size(); i++){
                    synth_count[blobInfo[i].type]++;
                    blobInfo[i].id = synth_count[blobInfo[i].type]+((blobInfo[i].type+1)*1000);
                    
                    sendOsc(blobInfo[i]);
                }

                for(int i=0; i<MAX_TYPES;i++) {
                    synth_count_old[i] = synth_count[i];
                    synth_count[i] = 0;
                }
            }

        }
        
        if(bSyncActive)
        {
            bool bSyncBlobsFound = false;
            
            if(bBlackVinyl)
            {
                mGrayTmp = mFonsLabel;
                mGrayTmp.mix(&mGrayLabel, 150, true);
                mGrayLabel = mGrayTmp;
            }else
            {
                mGrayLabel += mFonsLabel;
                mGrayLabel.invert();
            }
            mContourLabel.findContours(mGrayLabel, 30, (mCaptureWidth*mCaptureHeight)/3, MAX_BLOBS, false);
            
            for(int j=0; j<mContourLabel.nBlobs;j++) {
                
                isActive_blob_info( &(mContourLabel.blobs[j]), &(blobInfoSync));
                
                if(blobInfoSync.isActive)
                {
                    bSyncBlobsFound = true;
                    
                    
                    // 1st frame of SYNC blob crossing ?
                    if(bSyncBlobCrossing == false)
                    {
                        bSyncBlobCrossing = true;
                        // send SYNC via OSC
                        // ENVIO INFO A SC via OSC
                        if(mStatus == STAT_READY_SEND)
                            sendOscSync();
                    }
                }
            }
            // there is no blobs crossing the line
            if(bSyncBlobsFound == false)
                bSyncBlobCrossing = false;
		}
        
    }
    
}


//--------------------------------------------------------------
void testApp::draw()
{
    stringstream ss;
	string info;
    string s;
    
	ofSetHexColor(0xffffff);

//    int viewportWidth = ofGetViewportWidth();
//    int viewportHeight = ofGetViewportHeight();
    int viewportWidth = mCaptureWidth;
    int viewportHeight = mCaptureHeight;
    
    switch(vShowImage)
    {
        case SHOW_GRAY_VINYL:
            
            mGrayDiff.draw(ofRectangle(0, 0, viewportWidth, viewportHeight));

            ofPushMatrix();
            ofScale(viewportWidth / (float)mCaptureWidth, viewportHeight / (float)mCaptureHeight);
        
            for (size_t i = 0; i < blobInfo.size(); i++)
//            for (int i = 0; i < mContour.nBlobs; i++)
            {
                ofSetColor(255,255,255);
                if(blobInfo[i].isActive)
                {
                    ofFill();
                    ofSetColor(255,0,0);
                    ofRect(vinylCenter.x+blobInfo[i].min_pt.x,blobInfo[i].min_pt.y,5, blobInfo[i].max_pt.y-blobInfo[i].min_pt.y);
                    
                    ofSetColor(255, 255, 255);

                    int x=7;
/*
                    info = " g:"+ofToString(blobInfo[i].max_pt.y-blobInfo[i].min_pt.y,0);
                    ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
                    x=x+10;
                    info = " a:"+ofToString(blobInfo[i].area);
                    ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
                    x=x+10;
                    info = "ap:"+ofToString(blobInfo[i].area_per,3);
                    ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
                    x=x+10;
                    info = " l:"+ofToString(blobInfo[i].length);
                    ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
                    x=x+10;
                    info = "gm:"+ofToString(blobInfo[i].gr_medi);
                    ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
                    x=x+10;
                    info = " t:"+ofToString(blobInfo[i].tightness,3);
                    ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
                    x=x+10;
*/                    info = "po:"+ofToString(blobInfo[i].posX,2);
                    ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
                    x=x+10;
                    info = " r:"+ofToString(blobInfo[i].rad,2);
                    ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
                    x=x+10;

                    info = " id:"+ofToString(blobInfo[i].id);
                    ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
                    
/*                    x=x+10;
                    info = "color:"+ofToString((int)blobInfo[i].color.getHue())+","+ofToString((int)blobInfo[i].color.getBrightness())+","+ofToString((int)blobInfo[i].color.getSaturation());
                    ofDrawBitmapString(info, vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x);
 */
                    x=x+30;
                    ofSetColor(blobInfo[i].color.r, blobInfo[i].color.g, blobInfo[i].color.b);
                    ofRect(vinylCenter.x+blobInfo[i].min_pt.x+20,blobInfo[i].min_pt.y-x,70,12);
                    ofNoFill();
                }
            }
            
            for (int i = 0; i < mContour.nBlobs; i++)
                mContour.blobs[i].draw(0,0);

            ofPopMatrix();
        
            if (mShowHistograms) {
                ofFill();
                int maxHeight = viewportHeight / 2;
                ofColor col;
                int binWidth = viewportWidth / 3 / kNumLevels;

                for(int i=0; i<kNumLevels; i++) {
                    col.setHsb(i * kLevelSkip, 196, 128);
                    ofSetColor(col);
                    ofRect(0 + i * binWidth, viewportHeight - mFracHue[i] * maxHeight,
                        binWidth, mFracHue[i] * maxHeight);

                    col.setHsb(32, i * kLevelSkip, 160);
                    ofSetColor(col);
                    ofRect(viewportWidth / 3 + i * binWidth, viewportHeight - mFracSat[i] * maxHeight,
                       binWidth, mFracSat[i] * maxHeight);

                    col.setHsb(0, 0, i * kLevelSkip);
                    ofSetColor(col);
                    ofRect(2 * viewportWidth / 3 + i * binWidth, viewportHeight - mFracBri[i] * maxHeight,
                       binWidth, mFracBri[i] * maxHeight);
                }
            }
            
            ofNoFill();
            ofSetColor(75,75,75);
            ofCircle(vinylCenter.x, vinylCenter.y, vinylRad);
            ofCircle(vinylCenter.x, vinylCenter.y, labelRad);
            
            break;
            
        case SHOW_GRAY_SYNC:
            mGrayLabel.draw(0,0);
            for (int i = 0; i < mContourLabel.nBlobs; i++)
                mContourLabel.blobs[i].draw(0,0);
                
            ofNoFill();
            ofSetColor(75,75,75);
//            ofCircle(vinylCenter.x, vinylCenter.y, vinylRad);
            ofCircle(vinylCenter.x, vinylCenter.y, labelRad);
            break;
                
        case SHOW_COLOR:
            // muestra la camara
            mColor.draw(ofRectangle(0, 0, viewportWidth, viewportHeight));
                
            ofPushMatrix();
            ofScale(viewportWidth / (float)mCaptureWidth, viewportHeight / (float)mCaptureHeight);
            break;

	}

    switch(mStatus)
    {
        case  STAT_NONE:
            s = "Click on Vinyl Center";
            ofSetColor(80, 30, 50);
            mFont.drawString(s, 250+2, 20+2);
            ofSetColor(240, 100, 150);
            mFont.drawString(s, 250, 20);
        break;

        case  STAT_CENTER_CLICKED:
            s = "Click on Sync edge";
            ofSetColor(80, 30, 50);
            mFont.drawString(s, 250+2, 20+2);
            ofSetColor(240, 100, 150);
            mFont.drawString(s, 250, 20);
            break;

        case  STAT_LABEL_CLICKED:
            s = "Click on Vinyl edge";
            ofSetColor(80, 30, 50);
            mFont.drawString(s, 250+2, 20+2);
            ofSetColor(240, 100, 150);
            mFont.drawString(s, 250, 20);
            break;
    }


    if (mSettingCornerId < 0) {
        // We are not setting corners...
        
        if (mShowHistograms) {
            //
            // Draw the palette of colours, for calibration
            //
            int colorRadius = min(viewportWidth / 42, viewportHeight / 32);
            int x = viewportWidth - colorRadius * 2;
            int y = colorRadius * 2;
            
            ofFill();
            for (size_t i = 0; i < mColorData.size(); i++) {
                y = colorRadius * 2 + ((mColorData.size() - i)) * colorRadius * 2;
                ofColor col;
                float hue = mColorData[i].mHue.getMean();
                if (hue < 0)
                    hue += 360;
                hue *= 255.0 / 360.0;
                col.setHsb(hue, mColorData[i].mSat.getMean(), mColorData[i].mBri.getMean());
                ofSetColor(col);
                ofCircle(x, y, colorRadius);
                ofSetColor(255,255,255);
                mFontSmall.drawString(mColorData[i].mName, x - colorRadius - 60, y);
            }
            
            string str = "Click to:  (set WB)\n         (reset WB)";
            int posX = viewportWidth - mFontSmall.stringWidth(str) - 5;
            int posY = viewportHeight - 5 - 6 * mFontSmall.getLineHeight();
            mWhiteBalanceRect = mFontSmall.getStringBoundingBox(str, posX, posY);
            ofSetColor(60, 60, 60);
            mFontSmall.drawString(str, posX + 2, posY + 2);
            ofSetColor(240, 240, 240);
            mFontSmall.drawString(str, posX, posY);
            
            str = "Click to: (set corners)";
            posY = viewportHeight - 5 - 8 * mFontSmall.getLineHeight();
            mResetCornersRect = mFontSmall.getStringBoundingBox(str, posX, posY);
            ofSetColor(60, 60, 60);
            mFontSmall.drawString(str, posX + 2, posY + 2);
            ofSetColor(240, 240, 240);
            mFontSmall.drawString(str, posX, posY);
        }
    }
    else {
        // We are setting corners, give some instructions:
        int posX = viewportWidth / 5;
        int posY = viewportHeight / 4;
        stringstream ss;
        ss << "Capturing corners -- clockwise from top left:\nClick inside workspace corners\n\n         ";
        switch (mSettingCornerId) {
            case 0:
                ss << "TOP LEFT";
                break;
            case 1:
                ss << "TOP RIGHT";
                break;
            case 2:
                ss << "BOTTOM RIGHT";
                break;
            case 3:
                ss << "BOTTOM LEFT";
                break;
        }
        ofSetColor(80, 30, 50);
        mFont.drawString(ss.str(), posX+2, posY+2);
        ofSetColor(240, 100, 150);
        mFont.drawString(ss.str(), posX, posY);
    }
    
    if (mShowHelp) {
        ss.str("");
        ss.clear();
        ss << "'h' toggles histogram\n'?' toggles help\n<backspace> sends notes-off\n";

        int posX =  3 * viewportWidth / 5;
        int posY = viewportHeight - 5;
        mFontSmall.drawString(ss.str(), posX, posY - 4 * mFontSmall.getLineHeight());
    }
    
    s = "Thres:"+ofToString(mThreshold);
    ofSetColor(80, 30, 50);
    mFont.drawString(s, 5+2, 20+2);
    ofSetColor(240, 100, 150);
    mFont.drawString(s, 5, 20);
    
    if(bSyncActive)
    {
        s = "SYNC";
        ofSetColor(80, 30, 50);
        mFont.drawString(s, 100+2, 20+2);
        ofSetColor(240, 100, 150);
        mFont.drawString(s, 100, 20);
    }
    
    if(bVinylActive)
    {
        s = "TOCATA";
        ofSetColor(80, 30, 50);
        mFont.drawString(s, 100+2, 40+2);
        ofSetColor(240, 100, 150);
        mFont.drawString(s, 100, 40);
    }
}

string testApp::logStats(const StatsBase &inStats) const
{
    stringstream ss;
    ss << ofToString(inStats.getMean(), 1) << " " << ofToString(inStats.getVariance(), 3);
    return ss.str();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key)
{


	// gray threshold
	if(key == OF_KEY_UP) {
		mThreshold ++;
		if(mThreshold > 255) mThreshold = 255;
        saveSettings("wuTocataSetup.txt");
        cout << mThreshold << " ";
	}
	if(key == OF_KEY_DOWN) {
		mThreshold --;
		if(mThreshold < 0) mThreshold = 0;
        saveSettings("wuTocataSetup.txt");
       cout << mThreshold << " ";
	}

    if(key == 'q') {
        mWorkspaceCorners[0].set(0, 0);
        mWorkspaceCorners[1].set(mCaptureWidth, 0);
        mWorkspaceCorners[2].set(mCaptureWidth, mCaptureHeight);
        mWorkspaceCorners[3].set(0, mCaptureHeight);
        mSettingCornerId = -1;
        saveSettings("wuTocataSetup.txt");
    }
	if(key == 'v')
    {
        vShowImage ++;
        if(vShowImage > NUM_SHOWS-1) vShowImage = 0;
    }
    
	if(key == 'm') mMaInvisible = !mMaInvisible;
    
	if(key == ' ')
    {
        for (size_t i = 0; i < blobInfo.size(); i++)
            cout << blobInfo[i].posX << " ";
    }

	if(key == 's') 	{
        if(bVinylActive && !bSyncActive)
        {
            bVinylActive = false;
            bSyncActive = true;
        }else if(!bVinylActive && bSyncActive)
        {
            bVinylActive = true;
            bSyncActive = true;
        }else if(bVinylActive && bSyncActive)
            bSyncActive = false;
    }

    if(key == '?') mShowHelp = !mShowHelp;

    if(key == 'i') {
        // Dump the color info stats for the current blob
        ofLog() << "Current blob stats (mean and variance for R/G/B)" << endl <<
        logStats(mStatsHue) << endl << logStats(mStatsSat) << endl << logStats(mStatsBri);
    }

    if(key == 'h')
        mShowHistograms = !mShowHistograms;

    if(key == 'c') { // setvinyl CENTER
		vinylCenter.x = mouseX;
		vinylCenter.y = mouseY;
        if(mStatus == STAT_NONE)
            mStatus = STAT_CENTER_CLICKED;
	}
						
	if(key == 'l') { // set LABEL RADIUS
		labelRad = sqrt((mouseX-vinylCenter.x)*(mouseX-vinylCenter.x) + (mouseY-vinylCenter.y)*(mouseY-vinylCenter.y));
        if(bBlackVinyl)
            mFonsLabel.set_antiTocata(vinylCenter.x,vinylCenter.y, 20, labelRad,mCaptureWidth, mCaptureHeight, true);
        else
            mFonsLabel.set_antiTocata(vinylCenter.x,vinylCenter.y, 20, labelRad,mCaptureWidth, mCaptureHeight, false);
        
        if(mStatus == STAT_CENTER_CLICKED)
            mStatus = STAT_LABEL_CLICKED;
	}
	if(key == 't') { // set VINYL RADIUS
        vinylRad = sqrt((mouseX-vinylCenter.x)*(mouseX-vinylCenter.x) + (mouseY-vinylCenter.y)*(mouseY-vinylCenter.y));
        if(bBlackVinyl)
            mFons.set_antiTocata(vinylCenter.x,vinylCenter.y, labelRad, vinylRad,mCaptureWidth, mCaptureHeight, true);
        else
            mFons.set_antiTocata(vinylCenter.x,vinylCenter.y, labelRad, vinylRad,mCaptureWidth, mCaptureHeight, false);

        if(mStatus == STAT_LABEL_CLICKED)
            mStatus = STAT_READY_SEND;
	}

#ifdef USE_UVC_CAMERA
    if(key == 'w') {
        mWhiteBalance = mUvcControl.getWhiteBalance();
        ofLog() << "White Balance: " << mWhiteBalance;
    }
    if(key == 'W') mUvcControl.setAutoWhiteBalance(mUvcControl.getAutoWhiteBalance() == 0);
#endif

}

//--------------------------------------------------------------
void testApp::mouseMoved(int mouseX, int mouseY )
{
}

//--------------------------------------------------------------
void testApp::mouseDragged(int mouseX, int mouseY, int button)
{
}

//--------------------------------------------------------------
void testApp::mousePressed(int mouseX, int mouseY, int button)
{
    if (mStatus == STAT_NONE)
    {
        // calc vinyl Center
        vinylCenter.x = mouseX;
		vinylCenter.y = mouseY;
        mStatus = STAT_CENTER_CLICKED;
    }
    else if (mStatus == STAT_CENTER_CLICKED)
    {
        // calc Sync zone / label radius
		labelRad = sqrt((mouseX-vinylCenter.x)*(mouseX-vinylCenter.x) + (mouseY-vinylCenter.y)*(mouseY-vinylCenter.y));
        if(bBlackVinyl)
            mFonsLabel.set_antiTocata(vinylCenter.x,vinylCenter.y, 20, labelRad,mCaptureWidth, mCaptureHeight, true);
        else
            mFonsLabel.set_antiTocata(vinylCenter.x,vinylCenter.y, 20, labelRad,mCaptureWidth, mCaptureHeight, false);
        mStatus = STAT_LABEL_CLICKED;
    }
    else if (mStatus == STAT_LABEL_CLICKED)
    {
        // calc Turntable edge/Radius
        vinylRad = sqrt((mouseX-vinylCenter.x)*(mouseX-vinylCenter.x) + (mouseY-vinylCenter.y)*(mouseY-vinylCenter.y));
        if(bBlackVinyl)
            mFons.set_antiTocata(vinylCenter.x,vinylCenter.y, labelRad, vinylRad,mCaptureWidth, mCaptureHeight, true);
        else
            mFons.set_antiTocata(vinylCenter.x,vinylCenter.y, labelRad, vinylRad,mCaptureWidth, mCaptureHeight, false);
        mStatus = STAT_READY_SEND;
    }
    else if (mSettingCornerId >= 0) {
        int posX = mouseX * mCaptureWidth / ofGetViewportWidth();
        int posY = mouseY * mCaptureHeight / ofGetViewportHeight();
        mWorkspaceCorners[mSettingCornerId].set(posX, posY);
        mSettingCornerId++;
        if (mSettingCornerId > 3) {
            // We're done setting!  Save to file and get out of setting mode.
            saveSettings("wuTocataSetup.txt");
            mSettingCornerId = -1;
        }
    }
    else if (mShowHistograms) {
        if (mWhiteBalanceRect.inside(mouseX, mouseY)) {
            ofRectangle setRect(mWhiteBalanceRect);
            setRect.height /= 2;
            if (setRect.inside(mouseX, mouseY)) {
                setWhiteBalance();
            }
            else {
                // Reset white balance
                ofLog() << "Reset white balance.";
                mWBScaleRed = mWBScaleGreen = mWBScaleBlue = 1.0;
            }
            saveSettings("wuTocataSetup.txt");
        }
        if (mResetCornersRect.inside(mouseX, mouseY)) {
            // Turn on "set corners" mode
            mSettingCornerId = 0;
            return;
        }

        int colorRadius = min(ofGetViewportWidth() / 42, ofGetViewportHeight() / 32);
        int x = ofGetViewportWidth() - colorRadius * 2;
        int y;

        int dividingX = x - colorRadius;
        bool bAverage = (button != 0);
        float mixAmount = bAverage ? 0.5 : 0;
        if (mixAmount > 0) {
            ofLog() << "Mixing in color with fraction: " << mixAmount;
        }

        if (mouseX >= dividingX) {
            //
            // See if we clicked on the note "palette" to choose a new colour
            //
            for (size_t i = 0; i < mColorData.size(); i++) {
                y = colorRadius * 2 + ((mColorData.size() - i)) * colorRadius * 2;
                float dist = sqrt(pow(mouseX - x, 2.0) + pow(mouseY - y, 2.0));
                if (dist < colorRadius) {
                    ColorSpot &curColor = mColorData[i];
                    curColor.mHue.setMeanAndVariance(
                                                     (1 - mixAmount) * mStatsHue.getMean() + mixAmount * curColor.mHue.getMean(),
                                                     (1 - mixAmount) * mStatsHue.getVariance() + mixAmount * curColor.mHue.getVariance());
                    ofLog() << "Set hue to: " << curColor.mHue.getMean();
                    curColor.mSat.setMeanAndVariance(
                                                     (1 - mixAmount) * mStatsSat.getMean() + mixAmount * curColor.mSat.getMean(),
                                                     (1 - mixAmount) * mStatsSat.getVariance() + mixAmount * curColor.mSat.getVariance());
                    curColor.mBri.setMeanAndVariance(
                                                     (1 - mixAmount) * mStatsBri.getMean() + mixAmount * curColor.mBri.getMean(),
                                                     (1 - mixAmount) * mStatsBri.getVariance() + mixAmount * curColor.mBri.getVariance());

                    curColor.mClassifier.reset();
                    curColor.mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussCircularStat(curColor.mHue.getMean(), curColor.mHue.getVariance())));
                    curColor.mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(curColor.mSat.getMean(), curColor.mSat.getVariance())));
#ifdef USE_BRIGHTNESS_FEATURE
                    curColor.mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(curColor.mBri.getMean(), curColor.mBri.getVariance())));
#endif

                    curColor.mClassifier.setProb(0.5);  // Doesn't really matter what we put here for now,
                    // as long as they're all the same.

                    saveSettings("wuTocataSetup.txt");

                    break;
                }
            }
        }
    }
}

//--------------------------------------------------------------
void testApp::mouseReleased(int mouseX, int mouseY, int button)
{
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{
}

void testApp::setWhiteBalance()
{
    ofColor col;
    float hue = mStatsHue.getMean();
    if (hue < 0)
        hue += 360;
    hue *= 255.0 / 360.0;
    col.setHsb(hue, mStatsSat.getMean(), mStatsBri.getMean());
    float ref = min(min(col.r, col.g), col.b);
    
    mWBScaleRed = ref / col.r;
    mWBScaleGreen = ref / col.g;
    mWBScaleBlue = ref / col.b;
    ofLog() << "Set WB: " << mWBScaleRed << ", " << mWBScaleGreen << ", " << mWBScaleBlue;
}

ofColor testApp::calc_color(const ofxCvBlob * blob, unsigned char * colorPixels, unsigned char * maskPixels)
{
    
    ofColor mixColor;
    float hue, sat, bri;
    int counterHue[kNumLevels], counterSat[kNumLevels], counterBri[kNumLevels];
	int maxHue=0;
	int maxSat=0;
	int maxBri=0;
    
    for(int i=0; i<kNumLevels; i++) {
        counterHue[i] = 0;
        counterSat[i]=0;
        counterBri[i]=0;
    }
    
    int numPixels = 0;
    double rgbSums[3] = { 0, 0, 0 };
    
    mStatsHue.start();
    mStatsSat.start();
    mStatsBri.start();
    
    // compose final result
	for(int i=blob->boundingRect.x; i < blob->boundingRect.x + blob->boundingRect.width; i++) {
        //    for(int i=0; i<40; i++){
		for(int j=blob->boundingRect.y; j<blob->boundingRect.y + blob->boundingRect.height; j++){
            int mainPixelPos = (j*mCaptureWidth + i) * 3;		//pixel position of video
            int maskPixelPos = (j*mCaptureWidth + i);		//pixel position of mask
            
            ofColor initColor(colorPixels[mainPixelPos] * mWBScaleRed, colorPixels[mainPixelPos+1] * mWBScaleGreen, colorPixels[mainPixelPos+2] * mWBScaleBlue);
            if (maskPixels[maskPixelPos]>32) // només els pixels del blob
            {
                if (initColor.r + initColor.g + initColor.b < (248 * 3)) {
                    initColor.getHsb(hue, sat, bri);
                    mStatsHue.addDataPoint(initColor.getHue() * 360.0 / 255.0);  // convert from 0-255 to degrees
                    mStatsSat.addDataPoint(initColor.getSaturation());
                    mStatsBri.addDataPoint(initColor.getBrightness());
                    numPixels++;
                    rgbSums[0] += initColor.r;
                    rgbSums[1] += initColor.g;
                    rgbSums[2] += initColor.b;
                    counterHue[((int)hue) / kLevelSkip]++;
                    counterSat[((int)sat) / kLevelSkip]++;
                    counterBri[((int)bri) / kLevelSkip]++;
                }
            }
        }
    }
    
    // en realidad no és la media sino el color de pixel mas repetido
	for(int i=0; i < kNumLevels;i++)
	{
		if(counterHue[i] > maxHue)
		{
		    maxHue=counterHue[i];
			hue = (i + 0.5) * kLevelSkip;
		}
		if(counterSat[i] > maxSat)
		{
		    maxSat=counterSat[i];
			sat = (i + 0.5) * kLevelSkip;
		}
		if(counterBri[i] > maxBri)
		{
		    maxBri=counterBri[i];
			bri = (i + 0.5) * kLevelSkip;
		}
	}
    
    //    ofLog() << "H " << hue << " S " << sat << " B " << bri;
    if (numPixels == 0) {
        // Prevent divide by zero
        //ofLog() << "Num pixels == 0!";
        numPixels = 1;
    }
    mixColor.setHsb(hue, sat, bri);
    mixColor.set(0.5 * mixColor.r + 0.5 * rgbSums[0] / numPixels,
                 0.5 * mixColor.g + 0.5 * rgbSums[1] / numPixels,
                 0.5 * mixColor.b + 0.5 * rgbSums[2] / numPixels);
    //    mixColor.set(rgbSums[0] / numPixels,
    //                 rgbSums[1] / numPixels,
    //                 rgbSums[2] / numPixels);
    
    // Copy the blob's histogram for display, easing it into new values to smooth it out
    for(int i=0; i<kNumLevels; i++) {
        mFracHue[i] = 0.5 * mFracHue[i] + 0.5 * counterHue[i] / (double) numPixels;
        mFracSat[i] = 0.5 * mFracSat[i] + 0.5 * counterSat[i] / (double) numPixels;
        mFracBri[i] = 0.5 * mFracBri[i] + 0.5 * counterBri[i] / (double) numPixels;
    }
    
	return mixColor;
}

void testApp::isActive_blob_info(const ofxCvBlob * blob, BlobInfoSt * info)
{
    
    int offset = 0;   //default =15
    info->min_pt.y = 2000;
    info->max_pt.y = 0;
    info->isActive = false;
    bool ptdetectmax = false;
    bool ptdetectmin = false;
    bool amplesuf = false;
    
    // is synth?
    do{
        offset=offset+1;
        amplesuf = false;
        
        for(int i=0; i<blob->nPts; i++) {
            if(blob->pts[i].y > info->max_pt.y && blob->pts[i].x < vinylCenter.x+offset && blob->pts[i].x > vinylCenter.x-offset && blob->boundingRect.x < vinylCenter.x && (blob->boundingRect.x+blob->boundingRect.width)> vinylCenter.x)
            {
                info->max_pt.y = blob->pts[i].y;
                ptdetectmax = true;
            }
            if(blob->pts[i].y < info->min_pt.y && blob->pts[i].x < vinylCenter.x+offset && blob->pts[i].x > vinylCenter.x-offset && blob->boundingRect.x < vinylCenter.x && (blob->boundingRect.x+blob->boundingRect.width)> vinylCenter.x)
            {
                info->min_pt.y = blob->pts[i].y;
                ptdetectmin = true;
            }
        }
        
        if (ptdetectmax && ptdetectmin && (info->max_pt.y - info->min_pt.y)>6)
            amplesuf = true ;
        
    }while(!amplesuf && offset<45);
    
    if(ptdetectmax && ptdetectmin && (info->max_pt.y - info->min_pt.y)>0)
    {
        info->isActive = true;
    }
    
}


void testApp::get_blob_info(const ofxCvBlob * blob, BlobInfoSt * info)
{
    
    bool ptdetectmax = false;
    bool ptdetectmin = false;
    
    // detecta el gruix mitj�
    int count = 0;
    info->gr_medi = 0;
    int off_med = 1;
    
    for(int j=blob->boundingRect.x; j < blob->boundingRect.x + blob->boundingRect.width; j++) {
        
        int max_pt=0;
        int min_pt=2000;
        
        for(int i=0; i<blob->nPts; i++) {
            if(blob->pts[i].y > max_pt && blob->pts[i].x == j)
                //						if(blob->pts[i].y > max_pt && blob->pts[i].x < j+off_med && blob->pts[i].x > j-off_med)
            {
                max_pt = blob->pts[i].y;
                ptdetectmax = true;
            }
            if(blob->pts[i].y < min_pt && blob->pts[i].x == j)
                //						if(blob->pts[i].y < min_pt && blob->pts[i].x < j+off_med && blob->pts[i].x > j-off_med)
            {
                min_pt = blob->pts[i].y;
                ptdetectmin = true;
            }
        }
        if(ptdetectmax && ptdetectmin && max_pt-min_pt>4)
        {
            info->gr_medi = info->gr_medi + (max_pt-min_pt);
            count++;
        }
    }
    
    if( count>0)
        info->gr_medi = info->gr_medi/count;
    else
        info->gr_medi = 0;
    
    // altres valors
    info->tightness = blob->boundingRect.width/blob->boundingRect.height;
    info->area = blob->area;
    info->area_per = (blob->boundingRect.width*blob->boundingRect.height)/blob->area;
    info->length = blob->length;
    //				info->area_tight = info->area_per * info->tightness * 1000/info->area;
    info->gruix = info->max_pt.y-info->min_pt.y;
    info->posX = 1.-((vinylCenter.x-blob->boundingRect.x)/blob->boundingRect.width);
    info->rad = 1.-((info->max_pt.y - (vinylCenter.y+labelRad))/ (vinylRad-labelRad));

    // Here is where we decide which color label to give the blob
    double maxFeatDens = 0;
    int maxFeatIndex = 0;
    FeatureVec features;
#ifdef USE_BRIGHTNESS_FEATURE
    features.resize(3);
#else
    features.resize(2);
#endif
    for (size_t i = 0; i < mColorData.size(); i++) {
        const NaiveBayesClassifier &gc = mColorData[i].mClassifier;
        int feat = 0;
        features[feat++] = info->color.getHue() * 360.0 / 256.0;
        features[feat++] = info->color.getSaturation();
#ifdef USE_BRIGHTNESS_FEATURE
        features[feat++] = info->color.getBrightness();
#endif
        double dens = gc.posteriorDens(features);
        //ofLog() << "Class: " << mColorData[i].mName << " prob dens: " << dens;
        if (dens > maxFeatDens) {
            maxFeatDens = dens;
            maxFeatIndex = i;
        }
    }
    //ofLog() << "Decided it is " << mColorData[maxFeatIndex].mName;

    info->colortype = maxFeatIndex;
    
    // Decide type of blob/object
/*    if (isBlack)
    {
        info->type = OBJE;
        cout << OBJE << " ";
    }
    else if(isBlue)
    {
        info->type = PINT;
        cout << PINT << " ";
    }
    else if(isRed)
    {
        info->type = ROTU;
        cout << ROTU << " ";
    }
    else if(isGreen)
    {
        info->type = GRAIN;
        cout << GRAIN << " ";
    }
*/
    // de moment ho deixem aix�
    info->type = info->colortype;
}


void testApp::sendOsc(BlobInfoSt &info){
    ofxOscMessage m;
    m.setAddress("/tocata");
    m.addIntArg((int)(info.id/1000));
    m.addIntArg(info.id);
//    m.addFloatArg( ofMap(info.min_pt.y,0,ofGetHeight(), 0.0,1.0));
    m.addFloatArg( info.rad);
    m.addFloatArg( ofMap(info.gruix,0,ofGetHeight()/2, 0.0,1.0));
    m.addIntArg(info.gr_medi);
    m.addFloatArg( info.tightness);
    m.addIntArg(info.area);
    m.addFloatArg( info.area_per);
    m.addFloatArg( info.posX);
    
    mSender.sendMessage(m);
    
    
}

void testApp::sendOscSync(){
    ofxOscMessage m;
    m.setAddress("/sync");
    m.addIntArg(1);
    
    mSender.sendMessage(m);
}
void testApp::exit()
{
    // At exit, tell the server to turn all our notes off
    mInput.close();
}
