
#pragma once
#include "ofMain.h"
#include "ofxOpenCv.h"
#include "wuGrayImg.h"
#include "wuImagePlus.h"
#include "ofxOsc.h"
#include "gfGaussClassify.h"


#if 0 && defined(TARGET_OSX)
#define USE_UVC_CAMERA
#ifdef USE_UVC_CAMERA
#include "ofxUVC.h"
#endif
#endif // TARGET_OSX


#define MAX_BLOBS 8
#define MAX_TYPES 3

#define ROTU 0
#define PINT 1
#define OBJE 2
#define GRAIN 3

#define SHOW_COLOR 0
#define SHOW_GRAY_VINYL 1
#define SHOW_GRAY_SYNC 2
#define NUM_SHOWS 3

struct ColorSpot {
    string                  mName;
    int                     mDegree;
    CircularStats           mHue;
    LinearStats             mSat;
    LinearStats             mBri;
    NaiveBayesClassifier    mClassifier;
};

struct Note
{
    Note(int inDegree = 0, int inOct = 0)
    : degree(inDegree)
    , octave(inOct) {}

    int             degree;
    int             octave;
    ofPoint         pos;
};

struct TimeStep
{
    vector<Note>    mNotes;
};

struct BlobInfoSt
{
    BlobInfoSt()
    : isActive(false)
    , id(0)
    , gruix(0)
    , gr_medi(0)
    , type(0)
    , roughness(0)
    , tightness(1)
    , area(0)
    , area_per(0)
    , length(0)
    , posX(0)
    , rad(0){}
    
	bool isActive;
    int id;
	ofPoint min_pt;
	ofPoint max_pt;
	int gruix;  // gruix per on passa l' scan
	int gr_medi; // gruix mig del blob
	ofColor color;
	int type;
	int colortype;
	int roughness; // puntiagut o suau-arrodonit
	float tightness; // (inf-1)->estret horitzontal, (1) quadrat, (1-0) estret vertical
	int area;
	float area_per; // % d' area ocupada respecte l' area del rectangle
	int length;
	float posX;
    float rad;
};

enum {
    STAT_NONE,
    STAT_CENTER_CLICKED,
    STAT_LABEL_CLICKED,
    STAT_READY_SEND
};

// ------------------------------------------------- App
class testApp : public ofBaseApp {

public:

	void setup();
	void update();
	void draw();
    void exit();


	void keyPressed  (int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);

	ofColor calc_color(const ofxCvBlob * blob, unsigned char * colorPixels, unsigned char * maskPixels);
	void get_blob_info(const ofxCvBlob * blob, BlobInfoSt * info);
	void isActive_blob_info(const ofxCvBlob * blob, BlobInfoSt * info);
    void setWhiteBalance();

    bool loadSettings(const string &inFilename);
    void saveSettings(const string &inFilename);
    string logStats(const StatsBase &inStats) const;

    void sendOsc(BlobInfoSt &info);
    void sendOscSync();
	
	// OpenCV
	bool					mDrawBlobs;
	bool					mMaInvisible;
	bool                    mLearnBackground;
    bool                    bBlackVinyl;
	int						mThreshold;
	int						mCaptureWidth, mCaptureHeight;
    int                     mStatus;
    int                     vShowImage;
    bool                    bSyncActive; // sync detection ON/OFF
    bool                    bVinylActive; // color objects detection ON/OFF
    
	// CV images
	ofVideoGrabber			mInput;
#ifdef USE_UVC_CAMERA
    ofxUVC                  mUvcControl;
    float                   mWhiteBalance;
#endif

	ofxCvColorImage			mColor;
	wuImagePlus				mColorPlus;
	ofxCvGrayscaleImage		mGray;
	ofxCvGrayscaleImage		mGrayLabel;
	wuGrayImg               mFons, mFonsLabel, mGrayPlus, mGrayTmp;

	ofxCvGrayscaleImage 	mGrayDiff;

	// contour work
	ofxCvContourFinder		mContour;
	ofxCvContourFinder		mContourLabel;

	vector <BlobInfoSt>     blobInfo;
//	BlobInfoSt blobInfo[MAX_BLOBS];
    BlobInfoSt blobInfoSync;
    

    // Histogram stuff for the first blob
    const static int        kLevelSkip = 8;
    const static int        kNumLevels = 256 / kLevelSkip;
    double                  mFracHue[kNumLevels];
    double                  mFracSat[kNumLevels];
    double                  mFracBri[kNumLevels];
    CircularStats           mStatsHue;
    LinearStats             mStatsSat;
    LinearStats             mStatsBri;


	// tocata position variables
	ofPoint vinylCenter;
	int vinylRad, labelRad, numSynths;
	int synth_count[3], synth_count_old[3];

    // SYNC
    bool bSyncBlobCrossing;

    // Config / OSC stuff
    bool bSendToSC;
    ofxOscSender                mSender;
    string                      mSenderAddr;
    int                         mSenderPort;
    int                         mCameraId;
    int                         mMinArea;
    int                         mMaxArea;
    int                         mHighOctaveThresh;
    int                         mLowOctaveThresh;

    bool                        mShowHistograms;
    bool                        mShowHelp;
    vector<ColorSpot>           mColorData;
    NaiveBayesClassifier        mClassifier;
    ofRectangle                 mWhiteBalanceRect;
    ofRectangle                 mResetCornersRect;
    int                         mSettingCornerId;
    double                      mWBScaleRed;
    double                      mWBScaleGreen;
    double                      mWBScaleBlue;
    ofPoint                     mWorkspaceCorners[4];

    ofTrueTypeFont              mFont;
    ofTrueTypeFont              mFontSmall;


};
