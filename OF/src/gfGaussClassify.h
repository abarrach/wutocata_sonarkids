//
//  gfGaussClassify.h
//  SixPianosOpenCV
//
//  Created by Glen Fraser on 12/13/12.
//
//

#ifndef SixPianosOpenCV_gfGaussClassify_h
#define SixPianosOpenCV_gfGaussClassify_h

#include "ofMain.h"

class StatsBase {

protected:
    double  mMean;
    double  mVar;
    int     mNum;

public:
    StatsBase()
    : mMean(0)
    , mVar(0)
    , mNum(0) {}
    
    virtual void start() {
        mMean = mVar = mNum = 0;
    }
    
    virtual void addDataPoint(double curVal) = 0;

    virtual double getMean() const {
        return mMean;
    }
    
    virtual double getVariance() const {
        return mVar;
    }
    
    virtual double getNum() const {
        return mNum;
    }

    virtual void setMeanAndVariance(double inMean, double inVar) {
        mMean = inMean;
        mVar = inVar;
        // ? Let's pretend we got a lot of data...in theory people shouldn't subsequently "add"
        // to the data if they set it using this method
        mNum = 1000;
    }

};

class LinearStats : public StatsBase {

protected:
    double  mSumSqr;

public:
    LinearStats()
    : mSumSqr(0) {}

    virtual void start() {
        StatsBase::start();
        mSumSqr = 0;
    }
    // This is based on http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
    // Computes both mean and variance as the data come in.
    virtual void addDataPoint(double curVal) {
        mNum++;
        double delta = curVal - mMean;
        mMean += delta / mNum;
        mSumSqr += delta * (curVal - mMean);
        if (mNum > 1)
            mVar = mSumSqr/(mNum - 1);
        else
            mVar = 0;
    }
    
};


class CircularStats : public StatsBase {
    
protected:
    double  mSum[2];
    
public:
    CircularStats()
    {
        mSum[0] = mSum[1] = 0;
    }
    
    void start() {
        StatsBase::start();
        mSum[0] = mSum[1] = 0;
    }
    
    // This is not really right, but close enough for me... (-;
    // See: http://en.wikipedia.org/wiki/Wrapped_normal_distribution
    // and http://en.wikipedia.org/wiki/Von_Mises_distribution
    // Computes both mean and variance as the data come in.
    void addDataPoint(double curAngle) { // Angle should be in degrees
        mNum++;
        double vec[2];
        double ang = ofDegToRad(curAngle);
        vec[0] = cos(ang);
        vec[1] = sin(ang);
        mSum[0] += vec[0];
        mSum[1] += vec[1];
        vec[0] = mSum[0] / mNum;
        vec[1] = mSum[1] / mNum;
        mMean = ofRadToDeg(atan2(vec[1], vec[0]));
        // This is not quite correct, but close enough for me... (-;
        // See: http://en.wikipedia.org/wiki/Wrapped_normal_distribution
        // and http://en.wikipedia.org/wiki/Von_Mises_distribution
        if (mNum > 1) {
            mVar = 1 / (mNum / (mNum - 1) * (pow(vec[0],2) + pow(vec[1],2) - (1.0 / (mNum - 1))));
        }
        else {
            mVar = 1e6;
        }
        //
        // Here is what I actually am using to compute the angular variance:
        //
        double R = pow(vec[0], 2) + pow(vec[1],2);
        if (R > 0.999285969)
            R = 0.999285969;  // To prevent inverse of variance > 700 (overflow with prob. distrib. func.)
        else if (R < 7.1245e-218)
            R = 7.1245e-218;

        mVar = -2 * log(R);
        // mVar = (1.0 - R) / (2.0 * pow(R,2));
        // if (abs(diff) > 1e-6)
        //      mVar = 1 / diff;
        // else
        //      mVar = 1.0e6;
    }
};


double getMean(int n, const double *pX);
double getSampleVariance(int n, const double *pX);
void getMeanVariance(int n, const double *pX, double &outMean, double &outVariance); // "online" version

class GaussStatBase {
    
public:
    GaussStatBase(double inM = 0, double inV = 0)
    : mMean(inM)
    , mVariance(inV) {}
    
    virtual double evalGaussian(double x) const = 0;
    
protected:
    double mMean;
    double mVariance;
};

class GaussLinearStat : public GaussStatBase {
    
public:
    GaussLinearStat(double inM = 0, double inV = 0)
    : GaussStatBase(inM, inV) {}
    
    virtual double evalGaussian(double x) const
    {
        return 1.0 / sqrt(2 * M_PI * mVariance) * exp(-pow(x - mMean, 2) / (2 * mVariance));
    }
    
};

class GaussCircularStat : public GaussStatBase {
    
public:
    GaussCircularStat(double inM = 0, double inV = 0)
    : GaussStatBase(inM, inV) {}
    
    virtual double evalGaussian(double x) const
    {
        // We assume the angle 'x' is in degrees
        
        // Calculate something like the circular von Mises probability distribution function
        //
        // If we are going to normalize relative to the maximum value, then we don't
        // need to have the Bessel factor in there.  In this case, the value
        // always reaches 1 at the mean. In reality, it should vary between 1/2pi
        // (for kappa == 0) and a number much > 1 (for large kappa, like 700)
        double kappa;
        if (mVariance < (1.0 / 700))
            kappa = 700;
        else
            kappa = 1.0 / mVariance;
        return 1.0 / (2 * M_PI * approxIv0(kappa)) * exp(kappa * (cos(ofDegToRad(x - mMean))));
//        return exp(kappa * (cos(ofDegToRad(x - mMean)) - 1));
        
        // The real calculation (including Bessel) should be:
        // (where iv is the modified Bessel function)
        // return 1/(2*pi*iv(0, kappa))*exp(kappa*cos(ofDegToRad(x - mMean)));

    }

protected:
    double approxIv0(double kappa) const {
        // Very rough approximation to modified besselI(0, X)
        // Use a better integrator to see if we can use fewer steps...
        // Once kappa is large it deviates quite a bit from the real result
        double sum = 0;
        double step = M_PI/1000.0;
        for (double theta = 0; theta < M_PI; theta += step) {
            sum += exp(kappa * cos(theta)) * step;
        }
        return sum / M_PI;
    }
    
    // N.B. the derivative of exp(kappa * cos(theta)) is:
    //    kappa * sin(theta) * -exp(kappa * cos(theta))
};

typedef vector<ofPtr<GaussStatBase> > FeatureStatVec;
typedef vector<double> FeatureVec;

// From Bayesian rule, P(Y | X) == P(X | Y) * P(Y) / P(X)
// This is equivalent to finding Y where P(X | Y) * P(Y) is maximized.
// See: http://horicky.blogspot.com.au/2012/06/predictive-analytics-neuralnet-bayesian.html
class NaiveBayesClassifier {
public:
    NaiveBayesClassifier()
    : mProb(0) {}

    void reset()
    {
        mFeatureStats.clear();
        mProb = 0;
    }
    void addFeature(ofPtr<GaussStatBase> inFeat)
    {
        mFeatureStats.push_back(inFeat);
    }
    void setProb(double inProb)
    {
        mProb = inProb;
    }
        
    // See http://en.wikipedia.org/wiki/Naive_Bayes_classifier
    double posteriorDens(const FeatureVec &features) const
    {
        double dens = mProb;
        for (size_t i = 0; i < mFeatureStats.size(); i++) {
            ofPtr<GaussStatBase> pFs = mFeatureStats[i];
            double curDens = pFs->evalGaussian(features[i]);
            //1.0 / sqrt(2 * M_PI * fs.variance) * exp(-pow(features[i] - fs.mean, 2) / (2 * fs.variance));
            //ofLog() << i << " -- mean " << fs.mean << ", var " << fs.variance << ", dens: " << curDens;
            dens *= curDens;
        }
        return dens;
    }

protected:
    FeatureStatVec  mFeatureStats;
    double          mProb;
};

typedef map<string, NaiveBayesClassifier> NaiveBayesMap;
typedef NaiveBayesMap::const_iterator NaiveBayesMapCIt;

class GaussTest {
    NaiveBayesMap  mClasses;
    
public:
    void setupTest();
    string predictClass(const FeatureVec &features);
};

#endif
